import React, { Component } from 'react';
import ArticleList from './ArticleList';
import PropTypes from 'prop-types';

// import { data } from '../testData';
// const api = new DataApi(data);

class App extends Component {
  static childContextTypes = {
    store: PropTypes.object
  }
  getChildContext() {
    return {
      store: this.props.store
    }
  }
  
  state = this.props.store.getState();

  render() {
    return (
      <div>
        <ArticleList articles={this.state.articles} store={this.props.store} />
      </div>
    );
  }
}

export default App;